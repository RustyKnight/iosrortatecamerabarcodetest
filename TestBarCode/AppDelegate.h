//
//  AppDelegate.h
//  TestBarCode
//
//  Created by Shane Whitehead on 26/10/2014.
//  Copyright (c) 2014 KaiZen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

