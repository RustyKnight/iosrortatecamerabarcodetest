//
//  main.m
//  TestBarCode
//
//  Created by Shane Whitehead on 26/10/2014.
//  Copyright (c) 2014 KaiZen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
	}
}
