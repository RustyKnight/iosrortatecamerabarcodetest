//
//  ViewController.m
//  TestBarCode
//
//  Created by Shane Whitehead on 26/10/2014.
//  Copyright (c) 2014 KaiZen. All rights reserved.
//

#import "ViewController.h"
@import AVFoundation;
@import CoreGraphics;
@import UIKit;
@import QuartzCore;

@interface ViewController () <AVCaptureMetadataOutputObjectsDelegate>
@property (weak, nonatomic) IBOutlet UIView *avPreviewView;
@property (weak, nonatomic) IBOutlet UILabel *labelBarCode;

@end

@implementation ViewController {
	
	AVCaptureSession *_session;
	AVCaptureDevice *_device;
	AVCaptureDeviceInput *_input;
	AVCaptureMetadataOutput *_output;
	AVCaptureVideoPreviewLayer *_previewLayer;
	
	// Used to show what the camera has detected
	CALayer *_highlightView;
	CALayer *_blueHighlightView;
	
}

- (void)viewDidLoad {
	
	[super viewDidLoad];
	
	[self initCamera];
	
	if (_session) {
		
		_output = [[AVCaptureMetadataOutput alloc] init];
		[_output setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
		[_session addOutput:_output];
		
		_output.metadataObjectTypes = [_output availableMetadataObjectTypes];
		
	}
	
	_highlightView = [[CALayer alloc] init];
	_highlightView.borderColor = [UIColor redColor].CGColor;
	_highlightView.borderWidth = 3;
	[self.avPreviewView.layer addSublayer:_highlightView];

	_blueHighlightView = [[CALayer alloc] init];
	_blueHighlightView.borderColor = [UIColor blueColor].CGColor;
	_blueHighlightView.borderWidth = 3;
	[self.avPreviewView.layer addSublayer:_blueHighlightView];
	
}

-(void)bringLayerToFront:(CALayer*)layer {
	
	CALayer* parent = [layer superlayer];
	[parent insertSublayer:layer atIndex:[parent.sublayers count]];
//	NSLog(@"%@", [layer superlayer]);
	
}

-(void)viewWillAppear:(BOOL)animated {
	
	[super viewWillAppear:animated];
	[self startCamera];
	
}

-(void)viewDidDisappear:(BOOL)animated {
	
	[super viewDidDisappear:animated];
	[self stopCamera];
	
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection
{
	CGRect highlightViewRect = CGRectZero;
	AVMetadataMachineReadableCodeObject *barCodeObject;
	NSString *detectionString = nil;
	NSArray *barCodeTypes = @[AVMetadataObjectTypeUPCECode, AVMetadataObjectTypeCode39Code, AVMetadataObjectTypeCode39Mod43Code,
														AVMetadataObjectTypeEAN13Code, AVMetadataObjectTypeEAN8Code, AVMetadataObjectTypeCode93Code, AVMetadataObjectTypeCode128Code,
														AVMetadataObjectTypePDF417Code, AVMetadataObjectTypeQRCode, AVMetadataObjectTypeAztecCode];
	
	for (AVMetadataObject *metadata in metadataObjects) {
		for (NSString *type in barCodeTypes) {
			if ([metadata.type isEqualToString:type])
			{
				barCodeObject = (AVMetadataMachineReadableCodeObject *)[_previewLayer transformedMetadataObjectForMetadataObject:(AVMetadataMachineReadableCodeObject *)metadata];
				highlightViewRect = barCodeObject.bounds;
				detectionString = [(AVMetadataMachineReadableCodeObject *)metadata stringValue];
				break;
			}
		}
		
		if (detectionString != nil)
		{
			self.labelBarCode.text = detectionString;
			break;
		} else {
			self.labelBarCode.text = @"(none)";
		}
	}
	
	_highlightView.frame = highlightViewRect;
	_blueHighlightView.frame = highlightViewRect;
	[self bringLayerToFront:_highlightView];
	[self bringLayerToFront:_blueHighlightView];
	
}

#pragma mark - Camera

- (void)initCamera
{
	
	if (!_session) {
		
		if (!_device) {
			
			_device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
			
		}
		
		if (!_input) {
			
			NSError *error = nil;
			_input = [AVCaptureDeviceInput deviceInputWithDevice:_device error:&error];
			
			if (_input) {
				
				_session = [[AVCaptureSession alloc] init];
				[_session addInput:_input];
				
			} else {
				
				NSLog(@"Error %@", error);
				
			}
			
		}
		
	}
	
}

-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {

//	[self rotateLayerToOrientation:toInterfaceOrientation];
	[self updateCameraLayer];
	
}

- (void)startCamera
{
	
	if (_session) {
		
		[_session startRunning];
		
		if (!_previewLayer) {
			
			_previewLayer = [AVCaptureVideoPreviewLayer layerWithSession:_session];
			
//			CGRect layerRect = self.avPreviewView.layer.bounds;
//			_previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
//			[_previewLayer setBounds:layerRect];
//			[_previewLayer setPosition:CGPointMake(CGRectGetMidX(layerRect),CGRectGetMidY(layerRect))];
			
			[self updateCameraLayer];
			
		}
		
		[self.avPreviewView.layer addSublayer:_previewLayer];
//		[self rotateLayerToOrientation:[[UIApplication sharedApplication] statusBarOrientation]];
		
	}
	
}

- (void)stopCamera
{
	[_session stopRunning];
	[_previewLayer removeFromSuperlayer];
}

- (void)toggleCamera
{
	_session.isRunning ? [self stopCamera] : [self startCamera];
	[self.avPreviewView setNeedsDisplay];
	
}

-(void)updateCameraLayer {
	
	AVCaptureVideoOrientation videoOrientation;
	UIDeviceOrientation orientation =[[UIDevice currentDevice]orientation];
	_previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;

	switch (orientation) {
		case UIDeviceOrientationUnknown:
		case UIDeviceOrientationPortrait:
		case UIDeviceOrientationFaceUp:
		case UIDeviceOrientationFaceDown:
			videoOrientation = AVCaptureVideoOrientationPortrait;
			break;
		case UIDeviceOrientationPortraitUpsideDown:
			videoOrientation = AVCaptureVideoOrientationPortraitUpsideDown;
			break;
		case UIDeviceOrientationLandscapeLeft:
			videoOrientation = AVCaptureVideoOrientationLandscapeRight;
			break;
		case UIDeviceOrientationLandscapeRight:
			videoOrientation = AVCaptureVideoOrientationLandscapeLeft;
			break;
	}
	
	_previewLayer.connection.videoOrientation = videoOrientation;
	CGRect layerRect = self.avPreviewView.bounds;
	_previewLayer.frame = layerRect;
	[_previewLayer setPosition:CGPointMake(CGRectGetMidX(layerRect),CGRectGetMidY(layerRect))];
	
}

@end
